<?php
/**
Template Name: property
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Interview
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">


				<?php  			 

					$args = array(
		        	'post_type' => 'property',
		        	'tax_query' => array(
		                    array(
		                        'taxonomy' => 'status',
		                      	'field' => 'slug',
		            			'terms' => 'pre-construction'
		                    )
		                )
		        );


			query_posts( $args);
 ?>
			<?php while ( have_posts() ) : the_post(); ?>
					<?php the_post_thumbnail(); ?>
					<h3><?php the_title(); ?></h3>
					<p>  <?php the_content(); ?> </p>

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
