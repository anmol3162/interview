<?php
/**
 * Interview functions and definitions
 *
 * @package Interview
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'interview_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function interview_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Interview, use a find and replace
	 * to change 'interview' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'interview', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'interview' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'interview_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // interview_setup
add_action( 'after_setup_theme', 'interview_setup' );







function theme_setup() {

add_theme_support( 'post-thumbnails' );

}

add_action( 'after_setup_theme', 'theme_setup' );



add_action( 'init', 'codex_property_init' );



/**
 * Register a book post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function codex_property_init() {
	$labels = array(
		'name'               => _x( 'Property', 'post type general name', 'your-plugin-textdomain' ),
		'singular_name'      => _x( 'Property', 'post type singular name', 'your-plugin-textdomain' ),
		'menu_name'          => _x( 'Properties', 'admin menu', 'your-plugin-textdomain' ),
		'name_admin_bar'     => _x( 'Property', 'add new on admin bar', 'your-plugin-textdomain' ),
		'add_new'            => _x( 'Add New', 'Property', 'your-plugin-textdomain' ),
		'add_new_item'       => __( 'Add New Property', 'your-plugin-textdomain' ),
		'new_item'           => __( 'New Property', 'your-plugin-textdomain' ),
		'edit_item'          => __( 'Edit Property', 'your-plugin-textdomain' ),
		'view_item'          => __( 'View Property', 'your-plugin-textdomain' ),
		'all_items'          => __( 'All Properties', 'your-plugin-textdomain' ),
		'search_items'       => __( 'Search Properties', 'your-plugin-textdomain' ),
		'parent_item_colon'  => __( 'Parent Properties:', 'your-plugin-textdomain' ),
		'not_found'          => __( 'No Properties found.', 'your-plugin-textdomain' ),
		'not_found_in_trash' => __( 'No Properties found in Trash.', 'your-plugin-textdomain' )
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'Property' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'post-thumbnails' )
	);

	register_post_type( 'property', $args );
}



add_action( 'init', 'create_property_taxonomies', 0 );


function create_property_taxonomies() {
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Status', 'taxonomy general name' ),
		'singular_name'     => _x( 'Status', 'taxonomy singular name' ),
		'search_items'      => __( 'Search Status' ),
		'all_items'         => __( 'All Status' ),
		'parent_item'       => __( 'Parent Status' ),
		'parent_item_colon' => __( 'Parent Status:' ),
		'edit_item'         => __( 'Edit Status' ),
		'update_item'       => __( 'Update Status' ),
		'add_new_item'      => __( 'Add New Status' ),
		'new_item_name'     => __( 'New Genre Status' ),
		'menu_name'         => __( 'Status' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'Status' ),
	);

	register_taxonomy( 'status', array( 'property' ), $args );
}






/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function interview_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'interview' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'interview_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function interview_scripts() {
	wp_enqueue_style( 'interview-style', get_stylesheet_uri() );

	wp_enqueue_script( 'interview-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'interview-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'interview_scripts' );

/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
